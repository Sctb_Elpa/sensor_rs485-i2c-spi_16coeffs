/******************************************************************************/
/* System Level #define Macros                                                */
/******************************************************************************/

/* TODO Define system operating frequency */

/* Microcontroller MIPs (FCY) */
#ifndef F_CPU
#define F_CPU           10000000UL
#endif

#if defined(USE_PLL) && (USE_PLL == 1)
#define SYS_FREQ        (F_CPU * 4)
#else
#define SYS_FREQ        (F_CPU)
#endif

#define FCY             (SYS_FREQ/4)

#ifndef CRITICAL_TEMPERATURE
#define CRITICAL_TEMPERATURE    150.0
#endif

#define __PASSWORD__            17790 // 0 - 65535

/******************************************************************************/
/* System Function Prototypes                                                 */
/******************************************************************************/

/* Custom oscillator configuration funtions, reset source evaluation
functions, and other non-peripheral microcontroller initialization functions
go here. */

void ConfigureOscillator(void); /* Handles clock switching/osc initialization */
