/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

/******************************************************************************/
/* Configuration Bits                                                         */
/*                                                                            */
/* Refer to 'HI-TECH PICC and PICC18 Toolchains > PICC18 Configuration        */
/* Settings' under Help > Contents in MPLAB X IDE for available PIC18         */
/* Configuration Bit Settings for the correct macros when using the C18       */
/* compiler.  When using the Hi-Tech PICC18 compiler, refer to the compiler   */
/* manual.pdf in the compiler installation doc directory section on           */
/* 'Configuration Fuses'.  The device header file in the HiTech PICC18        */
/* compiler installation directory contains the available macros to be        */
/* embedded.  The XC8 compiler contains documentation on the configuration    */
/* bit macros within the compiler installation /docs folder in a file called  */
/* pic18_chipinfo.html.                                                       */
/*                                                                            */
/* For additional information about what the hardware configurations mean in  */
/* terms of device operation, refer to the device datasheet.                  */
/*                                                                            */
/* General C18/XC8 syntax for configuration macros:                           */
/* #pragma config <Macro Name>=<Setting>, <Macro Name>=<Setting>, ...         */
/*                                                                            */
/* General HiTech PICC18 syntax:                                              */
/* __CONFIG(n,x);                                                             */
/*                                                                            */
/* n is the config word number and x represents the anded macros from the     */
/* device header file in the PICC18 compiler installation include directory.  */
/*                                                                            */
/* A feature of MPLAB X is the 'Generate Source Code to Output' utility in    */
/* the Configuration Bits window.  Under Window > PIC Memory Views >          */
/* Configuration Bits, a user controllable configuration bits window is       */
/* available to Generate Configuration Bits source code which the user can    */
/* paste into this project.                                                   */
/*                                                                            */
/******************************************************************************/

#pragma config SOSCSEL = LOW, RETEN = OFF, INTOSCSEL = HIGH, XINST = OFF
#if defined(USE_PLL) && (USE_PLL == 1)
#pragma config PLLCFG = ON, FCMEN  = ON, IESO = ON, FOSC = HS1
#else
#pragma config PLLCFG = OFF, FCMEN  = ON, IESO = ON, FOSC = HS1
#endif
#pragma config BORPWR = ZPBORMV, BOREN = SBORDIS, BORV = 2, PWRTEN = ON
#pragma config STVREN = ON, BBSIZ = BB2K

#ifdef __DEBUG
#warning "Debug build!"
#	pragma config CANMX = PORTB, MSSPMSK = MSK7, MCLRE = ON
#	pragma config WDTPS = 128, WDTEN = NOSLP
#	pragma config CP0 = OFF, CP1 = OFF, CP2 = OFF, CP3 = OFF
#	pragma config CPB = OFF, CPD = OFF
#	pragma config WRT0 = OFF, WRT1 = OFF, WRT2 = OFF, WRT3 = OFF
#	pragma config WRTC = OFF, WRTB = OFF, WRTD = OFF
#	pragma config EBTR0 = OFF, EBTR1 = OFF, EBTR2 = OFF, EBTR3 = OFF
#	pragma config EBTRB = OFF
#else
#	pragma config CANMX = PORTB, MSSPMSK = MSK7, MCLRE = ON//OFF
#if defined(USE_PLL) && (USE_PLL == 1)
#	pragma config WDTPS = 64, WDTEN = NOSLP
#else
#	pragma config WDTPS = 64, WDTEN = NOSLP
#endif
#	pragma config CP0 = ON, CP1 = ON, CP2 = ON, CP3 = ON
#	pragma config CPB = ON, CPD = ON
#	pragma config WRT0 = ON, WRT1 = ON, WRT2 = ON, WRT3 = ON
#	pragma config EBTRB = ON
#	pragma config WRTC = ON, WRTB = ON, WRTD = OFF
#	pragma config EBTR0 = OFF, EBTR1 = OFF, EBTR2 = OFF, EBTR3 = OFF
#endif
