/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "dataModel.h"
#include "shaduler.h"
#include "spi_io.h"

#define DISABLE_START_INTERRUPT()   INTCONbits.INT0E = 0
#define ENABLE_START_INTERRUPT()    do {\
	INTCONbits.INT0IF = 0; \
	INTCONbits.INT0E = 1; \
} while (0)

static enum SPI_Mode mode;
static enum SPI_DataSegment segment = spi_pINPUTS;
static uint8_t nextbyte;
static uint8_t wPtr;

void enable_spi(BOOL enable)
{
    if (enable)
    {
        OpenSPI(SLV_SSON, MODE_01, SMPMID);

        SetPriorityIntSPI(SPI_HIGH_PRIO); // prio
        EnableIntSPI;
    }
    else
    {
        CloseSPI();
        DisableIntSPI;
    }
    mode = spi_IDLE;
    ENABLE_START_INTERRUPT();
}

static uint8_t getNextByte(uint8_t ptr)
{
	uint8_t nextbyte;
	if (ptr < sizeof(union unInputs))
	{
		nextbyte = ((uint8_t*)RegInputBuf.raw)[ptr];
	}
	else 
	{
		if (ptr < sizeof(union unInputs) + sizeof(union unDiscretInputs))
		{
			nextbyte = ((uint8_t*)DiscretInputsBuf.raw)[ptr - sizeof(union unInputs)];
		}
		else
		{
			if (ptr < sizeof(union unInputs) + 
						sizeof(union unDiscretInputs) +
						sizeof(union unCoils))
			{
				nextbyte = ((uint8_t*)CoilsBuf.raw)[ptr - (sizeof(union unInputs) +
														sizeof(union unDiscretInputs))];
			}
			else
			{
				if (ptr < sizeof(union unInputs) + 
						sizeof(union unDiscretInputs) +
						sizeof(union unCoils) +
						sizeof(union unHoldings))
					nextbyte = ((uint8_t*)RegHoldingBuf.raw)[ptr - (sizeof(union unInputs) +
														sizeof(union unDiscretInputs) + 
														sizeof(union unCoils))];
				else
					nextbyte = 0xff;
			}
		}
	}
	return nextbyte;
}

void SPI_ISR()
{
	uint8_t d = SSPBUF;
	SSPBUF =  nextbyte;
    ENABLE_START_INTERRUPT();

	nextbyte = getNextByte(ptr);
    ++ptr;

	switch (mode)
	{
	case spi_CMD:
		if ((d >= sizeof(union unInputs) + 
                sizeof(union unDiscretInputs)) && 
            (d < (sizeof(union unInputs) + 
                sizeof(union unDiscretInputs) +
                sizeof(union unCoils) +
                sizeof(union unHoldings))))
		{
			mode = spi_DATA;
			wPtr = d;	
		}
		else
			mode = spi_IDLE;
		break;
	case spi_DATA:
		if (wPtr < sizeof(union unInputs) + 
                sizeof(union unDiscretInputs) +
                sizeof(union unCoils))
		{
			((uint8_t*)CoilsBuf.raw)[wPtr - 
                    (sizeof(union unInputs)
                        + sizeof(union unDiscretInputs))] = d;
			shadule(processCoils_spii2c);
		}
		else if (wPtr < sizeof(union unInputs) + 
						sizeof(union unDiscretInputs) +
						sizeof(union unCoils) +
						sizeof(union unHoldings))
			{
				((uint8_t*)RegHoldingBuf_tmp.raw)[wPtr - 
                        (sizeof(union unInputs) +
                            sizeof(union unDiscretInputs) +
                            sizeof(union unCoils))] = d;
			}
		mode = spi_CMD;
		break;
	default:
		mode = spi_IDLE;
		break;
	}	
}

void SPI_Start_detected()
{
    // reset SPI
	ptr = 1;
	nextbyte = getNextByte(0);
    mode = spi_CMD;
    DISABLE_START_INTERRUPT();
}