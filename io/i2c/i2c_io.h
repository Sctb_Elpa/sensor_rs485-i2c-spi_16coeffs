/* 
 * File:   i2c_io.h
 * Author: tolyan
 *
 * Created on 12 ???? 2015 ?., 16:49
 */

#ifndef I2C_IO_H
#define	I2C_IO_H

#include <i2c.h>

#define D_A         (1 << 5)
#define S           (1 << 3)
#define P			(1 << 4)
#define R_W         (1 << 2)
#define BF          (1 << 0)

//https://books.google.ru/books?id=GKkMCqy7rEUC&pg=PA99&lpg=PA99&dq=pic+i2c+slave+code&source=bl&ots=AIpnl2xUVl&sig=MC-17TYarOf6ZvYa6Q_02Aw3uVA&hl=ru&sa=X&ei=Z0A_VcWgFIa_ywPw_YGYAw&ved=0CEMQ6AEwBTgK#v=onepage&q=pic%20i2c%20slave%20code&f=false
enum i2c_states
{
    I2C_STATE_1 = S | BF, // Write operation, last byte was an address, buffer is full.
    I2C_STATE_2 = S | D_A | BF, // Write operation, last byte was data, buffer is full.
    I2C_STATE_3 = S | R_W, // Read operation, last byte was an address,
	I2C_STATE_3_BF = I2C_STATE_3 | BF,
    I2C_STATE_4 = S | D_A | R_W, // Read operation, last byte was data, buffer is empty.
	I2C_STATE_4_BF = I2C_STATE_4 | BF,
    I2C_STATE_5 = S | D_A, // NACK received when sending data to the master

    I2C_STATE_MASK  = D_A | S | R_W | BF
};


enum I2C_DataSegment
{
    i2c_pHOLDING = 0,
    i2c_pINPUTS = 1,
    i2c_pCOILS = 2,
    i2c_pDINPUTS = 3
};

void enable_i2c(BOOL enable);
void I2C_ISR();

#endif	/* I2C_IO_H */

