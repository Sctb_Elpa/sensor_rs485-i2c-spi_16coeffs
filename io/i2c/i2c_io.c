/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "dataModel.h"
#include "port.h"
#include "shaduler.h"
#include "i2c_io.h"


static enum I2C_DataSegment segment;
static BOOL startMarker;
static uint8_t d;

void enable_i2c(BOOL enable) {
    if (enable) {
        TRISC |= (1 << 3) | (1 << 4); // i2c pins
        uint8_t i2c_addr = 0;
        i2c_addr = (PORTB & 0b1110) | (1 << 4);

        OpenI2C(SLAVE_7, SLEW_OFF); // slave 100KHz
        SSPADD = i2c_addr;

        // interupt
        SetPriorityIntI2C(0);
        EnableIntI2C;
    } else {
        CloseI2C();
        DisableIntI2C;
    }
}

static void fixPointer() {
    switch (segment) {
        case i2c_pHOLDING:
            if (ptr >= sizeof (union unHoldings))
                ptr = 0;
            break;
        case i2c_pINPUTS:
            if (ptr >= sizeof (union unInputs))
                ptr = 0;
            break;
        case i2c_pCOILS:
            if (ptr >= sizeof (union unCoils))
                ptr = 0;
            break;
        case i2c_pDINPUTS:
            if (ptr >= sizeof (union unDiscretInputs))
                ptr = 0;
            break;
    }
}

static void processi2c_Write() {
    switch (segment) {
        case i2c_pHOLDING:
        {
            // check rw mask
            uint8_t iRegIndex = PTR_TO_REGINDEX(ptr);
            ((uint8_t*) RegHoldingBuf_tmp.raw)[ptr++] = d;
            if (!(ptr & 1))
                holdingValidator();
            break;
        }
        case i2c_pCOILS:
            CoilsBuf.raw[ptr++] = d;
            shadule(processCoils_spii2c);
            break;
    }
    fixPointer();
    SSPCON1bits.CKP = 1;
}

static void sendDataByPtr() {
    switch (segment) {
        case i2c_pHOLDING:
            d = ((uint8_t*) RegHoldingBuf.raw)[ptr];
            break;
        case i2c_pINPUTS:
            d = ((uint8_t*) RegInputBuf.raw)[ptr];
            break;
        case i2c_pCOILS:
            d = CoilsBuf.raw[ptr];
            break;
        case i2c_pDINPUTS:
            d = DiscretInputsBuf.raw[ptr];
            break;
    }
    ++ptr;
    fixPointer();

    do {
        SSPCON1bits.WCOL = 0; //clear write collision flag
        SSPBUF = d;
    } while (SSPCON1bits.WCOL);
    SSPCON1bits.CKP = 1; // mast set after write to SSPBUF to release SCL
}

void I2C_ISR() {
    enum i2c_states state = SSPSTAT & I2C_STATE_MASK;
    if ((state & P) || !(state & S))
        state = I2C_STATE_5;

    switch (state) {
        case I2C_STATE_1:
            // transaction begins, master select as as target
            d = SSPBUF; // read address from sspbuf to tell module hat all ok

            SSPCON1bits.SSPOV = 0;
            SSPCON1bits.CKP = 1;
            SSPCON1bits.WCOL = 0;

            startMarker = 1;
            break;
        case I2C_STATE_2:
            // databyte was ressived
            d = SSPBUF; // read ressived byte
            SSPCON1bits.SSPOV = 0;
            if (startMarker) {
                // setAddr
                if (d & (1 << 7)) {
                    segment = i2c_pHOLDING;
                    ptr = (d & (~(1 << 7))) % sizeof (union unHoldings);
                } else {
                    if (d & (1 << 6)) {
                        if (d & 1 << 5) {
                            segment = i2c_pCOILS;
                            ptr = (d & ~0b11100000) % sizeof (union unCoils);
                        } else {
                            segment = i2c_pDINPUTS;
                            ptr = (d & ~0b11100000) % sizeof (union unDiscretInputs);
                        }
                    } else {
                        segment = i2c_pINPUTS;
                        ptr = (d & (~((1 << 7) | (1 << 6)))) % sizeof (union unInputs);
                    }
                }
                startMarker = 0;

                SSPCON1bits.CKP = 1;
            } else {
                //SSPCON1bits.CKP = 0; // problems there! don't enable!
                shadule(processi2c_Write);
            }
            break;
        case I2C_STATE_3:
        case I2C_STATE_3_BF:
            // master requests data (first byte)
            SSPCON1bits.CKP = 0; // operation in progreess master mast wait
            shadule(sendDataByPtr/*(*/);
            break;
        case I2C_STATE_4:
        case I2C_STATE_4_BF:
            // state 4
            // master requests data (not first byte)
            SSPCON1bits.CKP = 0; // operation in progreess master mast wait
            shadule(sendDataByPtr/*(*/);
            break;
        case I2C_STATE_5:
            // master sends NAK
            d = SSPBUF;
            SSPCON1bits.SSPOV = 0;
            SSPCON1bits.CKP = 1;
            SSPCON1bits.WCOL = 0;
            segment = i2c_pHOLDING;
            ptr = 0;
#if 0
            while (1) // reset
            {
                TRISCbits.TRISC6 = 0;
                LATCbits.LATC6 = 1;
            };
#endif
            break;
        default:
            INTCONbits.GIEH = 0;
            while (1) {
#if 0
                TRISCbits.TRISC6 = 0;
                LATCbits.LATC6 = 1;
#endif
            }; // reset
    }
}
