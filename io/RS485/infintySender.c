#include <string.h>

#include "port.h"
#include "mbport.h"
#include "dataModel.h"
#include "infintySender.h"
#include "mbproto.h"
#include "mbcrc.h"
#include "mbrtu.h"

#include "dataModel.h"

extern BOOL modbusBUSY;
extern volatile USHORT usRcvBufferPos;
extern volatile UCHAR  ucRTUBuf[];

struct readInputsReq
{
    uint8_t device_Addr;
    uint8_t function_code;
    uint16_t startAddr;
    uint16_t regCount;
    uint16_t CRC16;
};

static struct readInputsReq fakeReq =
{
    DEFAULT_DEVICE_ADDR,
    MB_FUNC_READ_INPUT_REGISTER,
    offsetof(struct _names, Pressure),
    (sizeof(float) / sizeof(uint16_t) * 2) << 8,
    0
};

void sendReport()
{
#if 0
TRISCbits. TRISC6 = 0;
LATCbits.LATC6 = !LATCbits.LATC6;
#else
   BOOL busy = modbusBUSY;
   BOOL state = (STATE_RX_IDLE == eRcvState);
    if (CoilsBuf.names.InfinitySendMode && 
	(!busy) && 
	state)
        xMBPortEventPost( EV_FRAME_FORCE_SEND );
#endif
}

eMBErrorCode genForceRequest(UCHAR *pucRcvAddress, UCHAR **pucFrame,
        USHORT * pusLength )
{
    eMBErrorCode err;
    
    // fill ressive buffer fake request
    ENTER_CRITICAL_SECTION();
    fakeReq.device_Addr = RegHoldingBuf.names.Adress;
    fakeReq.CRC16 = usMBCRC16((UCHAR*)&fakeReq,
        sizeof(struct readInputsReq) - sizeof(uint16_t));
    memcpy(ucRTUBuf, (UCHAR*)&fakeReq, sizeof(struct readInputsReq));
    usRcvBufferPos = sizeof(struct readInputsReq);
    err = eMBRTUReceive(pucRcvAddress, pucFrame, pusLength);
    EXIT_CRITICAL_SECTION();

    return err;
}