/* 
 * File:   shaduler.h
 * Author: tolyan
 *
 * Created on 11 ???? 2015 ?., 17:05
 */

#ifndef SHADULER_H
#define	SHADULER_H

typedef void (*taskRoutine)(void);

struct Task
{
    uint16_t period;
    taskRoutine Routine;
};

void enableShaduler(BOOL enable);
void tick();
void ProcessShaduled();
BOOL shadule(taskRoutine routine);

#endif	/* SHADULER_H */

