/* 
 * File:   FreqMeters.h
 * Author: tolyan
 *
 * Created on 10 ???? 2015 ?., 9:47
 */

#ifndef FREQMETERS_H
#define	FREQMETERS_H

#include <stdint.h>
#include "system.h"

#define START_FREQ  				(100.0) // initial freq

#define ALIVE_COUNTER				(5)
#define FREQMETER_ISR_PRIO                      (1)
#define MASTER_CLOCK_FAST                       (0)

#if MASTER_CLOCK_FAST
#define MASTER_CLOCK_HZ                         (SYS_FREQ)
#else
#define MASTER_CLOCK_HZ                         (SYS_FREQ / 4)
#endif

#define REF_TIMER_ISR()				++RefFreqTimer_Ovf
#define	TEMPERATURE_TIMER_ISR()                 do { \
    if (TempTimerTarget_new.value) \
    { \
        temperatureData.masterFreqData[0].value = refVal.value; \
        TMR1H = TempTimerTarget_new.ss.REGISTER_H; \
        TMR1L = TempTimerTarget_new.ss.REGISTER_L; \
        TempTimerTarget_new.value = 0; \
    } \
    else \
    { \
        if (temperatureData.work) \
            --temperatureData.work; \
        else \
        { \
            temperatureData.masterFreqData[1].value = refVal.value; \
            T1CONbits.TMR1ON = 0; \
        } \
    } \
    TempAliveCounter = ALIVE_COUNTER; \
} while(0)
#define PRESSURE_TIMER_ISR()                    do { \
    if (PresTimerTarget_new.value) \
    { \
        pressureData.masterFreqData[0].value = refVal.value; \
        TMR0H = PresTimerTarget_new.ss.REGISTER_H; \
        NOP(); /* NEED THERE */  \
        TMR0L = PresTimerTarget_new.ss.REGISTER_L; \
        T0CONbits.TMR0ON = 1; \
        PresTimerTarget_new.value = 0; \
    } \
    else \
    { \
        if (pressureData.work) \
            --pressureData.work; \
        else \
        { \
            pressureData.masterFreqData[1].value = refVal.value; \
            T0CONbits.TMR0ON = 0; \
        } \
    } \
    PresAliveCounter = ALIVE_COUNTER; \
} while(0)

#define CATCH_MASTER_TIMER()                    \
    refVal.s.REGISTER = TMR3; \
    refVal.s.Extantion = RefFreqTimer_Ovf; 
/*
    T3CONbits.TMR3ON = 0; \
    refVal.s.REGISTER = TMR3; \
    if(PIR2bits.TMR3IF) \
    { \
        T3CONbits.TMR3ON = 1; \
        PIR2bits.TMR3IF = 0; \
        REF_TIMER_ISR(); \
        refVal.s.Extantion = RefFreqTimer_Ovf; \
    } else { \
        T3CONbits.TMR3ON = 1; \
        refVal.s.Extantion = RefFreqTimer_Ovf; \
    }
*/

union u16bitCounter
{
    struct
    {
        uint16_t REGISTER;
        uint16_t Extantion;
    } s;
	struct
	{
		uint8_t REGISTER_L;
		uint8_t REGISTER_H;
        uint16_t Extantion;
	} ss;
    uint32_t value;
};

union u8bitCounter
{
    struct
    {
        uint8_t         REGISTER;
        uint16_t        Extantion;
        uint8_t         _UNUSED;
    } s;
    uint32_t value;
};

struct mesureCounter
{
	union u16bitCounter	masterFreqData[2];
	uint16_t			work;
};

void FreqMetersEnable();

void RefTimer_ISR();
void TemperatureTimerISR();
void PressureTimerISR();
void catchMasterFreq(union u16bitCounter* val);
void ApplyHoldings();

extern struct mesureCounter temperatureData;
extern struct mesureCounter pressureData;

extern volatile uint16_t RefFreqTimer_Ovf;

extern union u16bitCounter TempTimerTarget_new;
extern union u16bitCounter PresTimerTarget_new;

extern uint8_t PresAliveCounter;
extern uint8_t TempAliveCounter;

#endif	/* FREQMETERS_H */

