/*
 * File:   modbus.h
 * Author: tolyan
 *
 * Created on 10 ???? 2015 ?., 10:14
 */

#ifndef DATA_MODEL_H
#define	DATA_MODEL_H

#include <stdint.h>

#define DEVICE_ID                       0xDBA0
/************************************************************************/
#define DEFAULT_DEVICE_ADDR             4
#define DEFAILT_DEVICE_SPEED            57600
#define DEFAULT_MESURE_TIME             1000 
#define DEFAULT_CYCLICAL_SEND_PERIOD    100
#define DEFAULT_MESURE_UNITS            0

#define DEFAULT_ENABLE_P_CHANEL         TRUE
#define DEFAULT_ENABLE_T_CHANEL         TRUE

#define T_COEFFS_COUNT                  3
#define P_COEFFS_COUNT                  16
/************************************************************************/

#define REG_INPUT_START                 (0)
#define REG_INPUT_NREGS                 (4 * 2 + 2 * 2 + 1)

#define REG_HOLDING_START               (0)
#define REG_HOLDING_NREGS               (6 + (2 + T_COEFFS_COUNT) * 2 + \
                                        (2 + P_COEFFS_COUNT) * 2 + 3 + 1 + 2 * 2)

#define COILS_START                     (0)
#define COILS_NCOILS                    (7)

#define DISCRET_INPUTS_START            (0)
#define DISCRET_INPUTS_NINPUTS          (5)

#ifndef test_bit
#define test_bit(REG, n)                (REG & (1 << n))
#endif

#define PTR_TO_REGINDEX(p)              (p >> 1) /* ==> p / sizeof(uint16_t)*/

enum enMeasureUnits {
    MU_kgs_cm2 = 0,
    MU_mmHg = 1,
    MU_gPa = 2,
    MU_kPa = 3,
    MU_MPa = 4,
    MU_mmH2O = 5,
    MU_psi = 6,

    MU_COUNT,
};

struct sTemperatureCoeffs
{
    float T0;
    float C[T_COEFFS_COUNT];
    float F0;
};

struct sPressureCoeffs
{
    float A[P_COEFFS_COUNT];
    float Ft0;
    float Fp0;    
};

#if REG_HOLDING_NREGS > 0
    union unHoldings {
        struct {
            uint16_t ID;
            uint16_t Serial;
            uint16_t Password;
            uint16_t PressureMesureTime;
            uint16_t TemperatureMesureTime;
            uint16_t MeasureUnits;

            struct sPressureCoeffs P_coeffs;
            struct sTemperatureCoeffs T_coeffs;
            
            // modbus only
            uint16_t Adress;
            uint16_t Speed;
            uint16_t cyclicalSendPeriod;
            
            uint32_t firmwareVersion;
            uint32_t firmware_hash;
        } names;
        uint16_t raw[REG_HOLDING_NREGS];
    };
    extern union unHoldings RegHoldingBuf;
	extern union unHoldings RegHoldingBuf_tmp;
    extern const uint8_t ucRegHoldongROMask[];
#endif

#if COILS_NCOILS > 0
    union unCoils {
        struct {
			unsigned    PresureEnabled:1;
			unsigned    TemperatureEnabled:1;
            unsigned    WriteSettings:1;
            unsigned    ApplySettings:1;
            unsigned    InfinitySendMode:1;
			unsigned    PowerSaveMode:1;
			unsigned    ResetDefaults:1;
        } names;
        uint8_t raw[(COILS_NCOILS - 1) / 8 + 1];
    };
    extern union unCoils CoilsBuf;
#endif

#if DISCRET_INPUTS_NINPUTS > 0
    union unDiscretInputs {
        struct {
			unsigned    PresureChanelFailure:1; 
			unsigned    TemperatureChanelFailure:1;
			unsigned    CryticalTemperature:1;
			unsigned    I2CMode:1;
			unsigned    SPIMode:1;
        } names;
        uint8_t raw[(DISCRET_INPUTS_NINPUTS - 1) / 8 + 1];
    };
    extern union unDiscretInputs DiscretInputsBuf;
#endif

#if REG_INPUT_NREGS > 0
    union unInputs {
        struct _names{
            float Pressure;
            float Temperature;
            float Fp;
            float Ft;
#if 0
            uint32_t start;
            uint32_t stop;
#endif
        } names;
        uint16_t raw[REG_INPUT_NREGS];
    };
#ifdef MUTE_RESET
extern persistent union unInputs RegInputBuf;
#else
extern union unInputs RegInputBuf;
#endif
#endif

struct EEPROMContainer
{
    uint16_t CRC;
#if REG_HOLDING_NREGS > 0
    union unHoldings holdings;
#endif
#if COILS_NCOILS > 0
    union unCoils Coils;
#endif
    uint16_t size;
};


#ifndef _MB_H
typedef enum
{
    MB_ENOERR,                  /*!< no error. */
    MB_ENOREG,                  /*!< illegal register address. */
    MB_EINVAL,                  /*!< illegal argument. */
    MB_EPORTERR,                /*!< porting layer error. */
    MB_ENORES,                  /*!< insufficient resources. */
    MB_EIO,                     /*!< I/O error. */
    MB_EILLSTATE,               /*!< protocol stack in illegal state. */
    MB_ETIMEDOUT                /*!< timeout error occurred. */
} eMBErrorCode;

typedef enum
{
    MB_REG_READ,                /*!< Read register values and pass to protocol stack. */
    MB_REG_WRITE                /*!< Update register values. */
} eMBRegisterMode;

eMBErrorCode eMBRegInputCB(uint8_t * pucRegBuffer, uint16_t usAddress,
        uint16_t usNRegs);
eMBErrorCode eMBRegHoldingCB(uint8_t * pucRegBuffer, uint16_t usAddress,
        uint16_t usNRegs, eMBRegisterMode eMode);
eMBErrorCode eMBRegCoilsCB(uint8_t * pucRegBuffer, uint16_t usAddress,
        uint16_t usNCoils, eMBRegisterMode eMode);
eMBErrorCode eMBRegDiscreteCB(uint8_t * pucRegBuffer, uint16_t usAddress,
        uint16_t usNDiscrete);
#endif

void processCoils();
void processCoils_spii2c();

void InitDataModel();
void saveSettings();
eMBErrorCode holdingValidator();

void save_byte();

float convertkgs_m2_TO(float mmHg);

uint8_t __div8(uint8_t val);
uint8_t __mod8(uint8_t val);

extern void (*processAfterAnsver)(void);
extern uint8_t ptr; // i2c/spi data pointer

#endif	/* DATA_MODEL_H */

