/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <string.h>

#include "user.h"
#include "dataModel.h"
#include "mbcrc.h"
#include "shaduler.h"
#include "port.h"
#include "system.h"

// #define MUTE_RESET // enable by -DMUTE_RESET

#if REG_HOLDING_NREGS > 0
union unHoldings RegHoldingBuf;
union unHoldings RegHoldingBuf_tmp;
const uint8_t ucRegHoldongROMask[] = {0b00000001, 0, 0, 0, 0, 0, 
    0b10000000, // firmwareVersion MSB
    0b00000111  // firmwareVersion MSB + firmware_hash
};
#endif

#if REG_INPUT_NREGS > 0
#ifdef MUTE_RESET
persistent union unInputs RegInputBuf;
#else
union unInputs RegInputBuf;
#endif
#endif

#if COILS_NCOILS > 0
union unCoils CoilsBuf;
#endif

#if DISCRET_INPUTS_NINPUTS > 0
union unDiscretInputs DiscretInputsBuf;
#endif

#ifndef set_bit
#define set_bit(REG, n)                 REG |= (1 << n)
#endif

#ifndef clear_bit
#define clear_bit(REG, n)               REG &= ~(1 << n)
#endif

static struct EEPROMContainer container_to_write;
static uint16_t save_counter = 0;

extern const uint32_t FW_VERSION;
extern const uint32_t FW_HASH;

eMBErrorCode    eMBClose( void );
eMBErrorCode    eMBEnable( void );
eMBErrorCode    eMBDisable( void );

void (*processAfterAnsver)(void) = NULL;

uint8_t ptr = 0;

static const float mmHgTOCoeffs[MU_COUNT - 1] = {
    735.55914,
    980.665012,
    98.0665012,
    0.0980665012,
    10000.0,
    14.223343
};

static void reset_saving() {
    save_counter = 0;
}

static void start_saving() {
    save_counter = sizeof(struct EEPROMContainer);
}

void save_byte() {
    /*
     * Don't even think about change this function
     * EEPROM_READ works very strange!
     * Breaks near variables, fo EEPROM_WRITE inlined
     */
    
    if (!save_counter) {
        return;
    }
    
    unsigned int pos = sizeof(struct EEPROMContainer) - save_counter;
    char currentvalue = EEPROM_READ(pos);
    char wb = ((char*)&container_to_write)[pos];
    
    if (currentvalue != wb) {
        char GIE_BIT_VAL = 0;
        EEADRH = (pos >> 8) & 0x03;
        EEADR = (pos & 0x0ff);
        EEDATA = wb;
        EECON1bits.EEPGD = 0;
        EECON1bits.CFGS = 0;
        EECON1bits.WREN = 1;
        GIE_BIT_VAL = INTCONbits.GIE;
        INTCONbits.GIE = 0;
        EECON2 = 0x55;
        EECON2 = 0xAA;
        EECON1bits.WR = 1;
        while(EECON1bits.WR);				//Wait till the write completion
        INTCONbits.GIE = GIE_BIT_VAL;
        EECON1bits.WREN = 0;
    }
    --save_counter;
}

static void cancel_write_to_protected_cells() {
    if (RegHoldingBuf.names.Serial != __PASSWORD__) {
        // A0 not protected
        float A0 = RegHoldingBuf_tmp.names.P_coeffs.A[0];
        // if saved_serial != password cancel write coefficients by copy prev value
        memcpy(&RegHoldingBuf_tmp.names.P_coeffs, &RegHoldingBuf.names.P_coeffs, 
                sizeof(struct sPressureCoeffs));
        memcpy(&RegHoldingBuf_tmp.names.T_coeffs, &RegHoldingBuf.names.T_coeffs, 
                sizeof(struct sTemperatureCoeffs));
        RegHoldingBuf_tmp.names.P_coeffs.A[0] = A0;
    }
}

void ApplyHoldings()
{
	if (holdingValidator() == MB_ENOERR) {
        cancel_write_to_protected_cells();
		memcpy(RegHoldingBuf.raw, RegHoldingBuf_tmp.raw, 
                sizeof(union unHoldings));
    } else {
        memcpy(RegHoldingBuf_tmp.raw, RegHoldingBuf.raw, 
                sizeof(union unHoldings));
    }
}

eMBErrorCode holdingValidator()
{
	uint8_t tmp;
	static const uint16_t valid_speeds[] = { 38400, 57600, 0xffff };

	if (RegHoldingBuf_tmp.names.ID != DEVICE_ID)
		goto __failed;

	if (RegHoldingBuf_tmp.names.PressureMesureTime < 20 ||
		RegHoldingBuf_tmp.names.PressureMesureTime > 5000)
		goto __failed;

	if (RegHoldingBuf_tmp.names.TemperatureMesureTime < 20 ||
		RegHoldingBuf_tmp.names.TemperatureMesureTime > 5000)
		goto __failed;
        
        if (RegHoldingBuf_tmp.names.MeasureUnits >= MU_COUNT)
                goto __failed;

	if (RegHoldingBuf_tmp.names.Adress == 0 || 
		RegHoldingBuf_tmp.names.Adress > 247)
		goto __failed;

	if (RegHoldingBuf_tmp.names.cyclicalSendPeriod < 20)
		goto __failed;

	for (tmp = 0; tmp < sizeof(valid_speeds) / sizeof(uint16_t); ++tmp)
		if (RegHoldingBuf_tmp.names.Speed == valid_speeds[tmp])
			goto __speed_ok;

__failed:
	memcpy(RegHoldingBuf_tmp.raw, RegHoldingBuf.raw, sizeof(union unHoldings)); // restore
	return MB_EINVAL;

__speed_ok:
	return MB_ENOERR;
}

uint8_t __div8(uint8_t val) {
    return val >> 3;
}

uint8_t __mod8(uint8_t val) {
    return val - (__div8(val) << 3);
}

static void __copy(uint8_t* to, uint8_t* from) {
    *to++ = *(++from);
    *to = *(--from);
}

static void saveSettingsContainer() 
{
    container_to_write.CRC = usMBCRC16((uint8_t*)&container_to_write + sizeof(uint16_t),
            sizeof(struct EEPROMContainer) - sizeof(uint16_t));
    start_saving();
}

static void resetDefaults()
{
    reset_saving();
    
    container_to_write.size = sizeof(container_to_write);
    memcpy(&container_to_write.holdings, &RegHoldingBuf, sizeof(union unHoldings));
    
    container_to_write.holdings.names.ID = DEVICE_ID;
    container_to_write.holdings.names.Adress = DEFAULT_DEVICE_ADDR;
    container_to_write.holdings.names.Speed = DEFAILT_DEVICE_SPEED;
    container_to_write.holdings.names.TemperatureMesureTime = DEFAULT_MESURE_TIME;
    container_to_write.holdings.names.PressureMesureTime = DEFAULT_MESURE_TIME;
    container_to_write.holdings.names.Password = 0;
    container_to_write.holdings.names.cyclicalSendPeriod = DEFAULT_CYCLICAL_SEND_PERIOD;
    container_to_write.holdings.names.MeasureUnits = DEFAULT_MESURE_UNITS;
    container_to_write.holdings.names.firmwareVersion = FW_VERSION;
    container_to_write.holdings.names.firmware_hash = FW_HASH;
    
    memset(&container_to_write.Coils, 0, sizeof (union unCoils));
    container_to_write.Coils.names.TemperatureEnabled = DEFAULT_ENABLE_P_CHANEL;
    container_to_write.Coils.names.PresureEnabled = DEFAULT_ENABLE_T_CHANEL;

     // write
    saveSettingsContainer();

    RESET();
}

static void resetDefaultsFirst() {
    struct EEPROMContainer container;

    container.size = sizeof(container);
    container.holdings.names.ID = DEVICE_ID;
    container.holdings.names.Serial = __PASSWORD__;
    container.holdings.names.Adress = DEFAULT_DEVICE_ADDR;
    container.holdings.names.Speed = DEFAILT_DEVICE_SPEED;
    container.holdings.names.TemperatureMesureTime = DEFAULT_MESURE_TIME;
    container.holdings.names.PressureMesureTime = DEFAULT_MESURE_TIME;
    container.holdings.names.Password = 0;
    memset(&container.holdings.names.T_coeffs, 0,
        sizeof(struct sTemperatureCoeffs));
    memset(&container.holdings.names.P_coeffs, 0,
        sizeof(struct sPressureCoeffs));
    container.holdings.names.T_coeffs.C[0] = 1;
    container.holdings.names.P_coeffs.A[3] = 1;
    container.holdings.names.cyclicalSendPeriod = DEFAULT_CYCLICAL_SEND_PERIOD;
    container.holdings.names.MeasureUnits = DEFAULT_MESURE_UNITS;
    container.holdings.names.firmwareVersion = FW_VERSION;
    container.holdings.names.firmware_hash = FW_HASH;
    
    memset(container.Coils.raw, 0, sizeof (union unCoils));
    container.Coils.names.TemperatureEnabled = DEFAULT_ENABLE_P_CHANEL;
    container.Coils.names.PresureEnabled = DEFAULT_ENABLE_T_CHANEL;
    
     // write defaults
    container.CRC = usMBCRC16(((uint8_t*)&container) + sizeof(uint16_t),
            sizeof(struct EEPROMContainer) - sizeof(uint16_t));
    for(size_t i = 0; i < sizeof(struct EEPROMContainer); ++i)
	{
		CLRWDT();
        EEPROM_WRITE(i, ((uint8_t*)&container)[i]);
	}

    RESET();
}

eMBErrorCode
eMBRegInputCB(uint8_t * pucRegBuffer, uint16_t usAddress, uint16_t usNRegs) {
#if (REG_INPUT_NREGS > 0)
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t iRegIndex = 0;

    --usAddress;

    if ((usAddress >= REG_INPUT_START) &&
            (usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS)) {
		iRegIndex = (uint16_t) (usAddress - REG_HOLDING_START);
        while (usNRegs-- > 0) {
            __copy(pucRegBuffer, (uint8_t*) & RegInputBuf.raw[iRegIndex]);
            iRegIndex++;
            pucRegBuffer += sizeof (uint16_t);
        }
    } else {
        eStatus = MB_ENOREG;
    }

    return eStatus;
#else
    return MB_ENOREG;
#endif
}

eMBErrorCode
eMBRegHoldingCB(uint8_t * pucRegBuffer, uint16_t usAddress, uint16_t usNRegs,
        eMBRegisterMode eMode) {
#if (REG_HOLDING_NREGS > 0)
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t iRegIndex;

    --usAddress;
    if ((usAddress >= REG_HOLDING_START) &&
            (usAddress + usNRegs <= REG_HOLDING_START + REG_HOLDING_NREGS)) {
        iRegIndex = (uint16_t) (usAddress - REG_HOLDING_START); 
        if (eMode == MB_REG_READ) { 
            while (usNRegs-- > 0) {
                __copy(pucRegBuffer, (uint8_t*) & RegHoldingBuf.raw[iRegIndex]);
                iRegIndex++;
                pucRegBuffer += sizeof (uint16_t);
            }
        } else {
            uint8_t i = usNRegs;
            while (i > 0) {
                if (test_bit(ucRegHoldongROMask[__div8(iRegIndex)], __mod8(iRegIndex)))
                    return MB_EINVAL;
                ++iRegIndex;
                --i;
            }

            iRegIndex = (uint16_t) (usAddress - REG_HOLDING_START);
            while (usNRegs-- > 0) {
                static uint8_t ii = 0;
                if (ii++)
                    ii = 0;
                __copy((uint8_t*) & RegHoldingBuf_tmp.raw[iRegIndex], pucRegBuffer);
                iRegIndex++;
                pucRegBuffer += sizeof (uint16_t);
            }
			eStatus = holdingValidator();
        }
    } else {
        eStatus = MB_ENOREG;
    }
    return eStatus;
#else
    return MB_ENOREG;
#endif
}

eMBErrorCode
eMBRegCoilsCB(uint8_t * pucRegBuffer, uint16_t usAddress, uint16_t usNCoils,
        eMBRegisterMode eMode) {
#if (COILS_NCOILS > 0)
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t iCoilIndex;
    uint16_t tmp = 0;
    --usAddress;
    if ((usAddress >= COILS_START)
            && (usAddress + usNCoils <= COILS_START + COILS_NCOILS)) {
        iCoilIndex = (uint16_t) (usAddress - COILS_START);
        if (eMode == MB_REG_READ) { // read
            memset(pucRegBuffer, 0, __div8(usNCoils) + 1);
            for (; tmp < usNCoils; ++tmp) {
                if (test_bit(CoilsBuf.raw[__div8(iCoilIndex)], __mod8(iCoilIndex)))
                    set_bit(*pucRegBuffer, __mod8(tmp));
                ++iCoilIndex;
                if (!__mod8(iCoilIndex))
                    ++pucRegBuffer;
            }
        } else {// write
            for (; tmp < usNCoils; ++tmp) {
                if (test_bit(pucRegBuffer[__div8(tmp)], __mod8(tmp)))
                    set_bit(CoilsBuf.raw[__div8(iCoilIndex)], __mod8(iCoilIndex));
                else
                    clear_bit(CoilsBuf.raw[__div8(iCoilIndex)], __mod8(iCoilIndex));
                ++iCoilIndex;
            }
            processCoils();
        }
    } else {
        eStatus = MB_ENOREG;
    }

    return eStatus;
#else
    return MB_ENOREG;
#endif
}

eMBErrorCode
eMBRegDiscreteCB(uint8_t * pucRegBuffer, uint16_t usAddress, uint16_t usNDiscrete) {
#if (DISCRET_INPUTS_NINPUTS > 0)
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t iDiscreteIndex;
    --usAddress;
    //??????
    if ((usAddress >= COILS_START) &&
            (usAddress + usNDiscrete <= DISCRET_INPUTS_START + DISCRET_INPUTS_NINPUTS)) {
        uint16_t tmp = 0;
        iDiscreteIndex = (uint16_t) (usAddress - DISCRET_INPUTS_START);
        memset(pucRegBuffer, 0, __div8(usNDiscrete) + 1);
        for (; tmp < usNDiscrete; ++tmp) {
            if (test_bit(DiscretInputsBuf.raw[__div8(iDiscreteIndex)], __mod8(iDiscreteIndex)))
                set_bit(*pucRegBuffer, __mod8(tmp));
            ++iDiscreteIndex;
            if (!__mod8(iDiscreteIndex))
                ++pucRegBuffer;
        }
    } else {
        eStatus = MB_ENOREG;
    }
    return eStatus;
#else
    return MB_ENOREG;
#endif
}

void InitDataModel()
{
    uint16_t i;

    // read eeprom
    struct EEPROMContainer container;
    for(i = 0; i < sizeof(struct EEPROMContainer); ++i)
        ((uint8_t*)&container)[i] = EEPROM_READ(i);

    // check
	CLRWDT();
    if (usMBCRC16((uint8_t*)&container + sizeof(uint16_t),
        sizeof(struct EEPROMContainer) - sizeof(uint16_t)) != container.CRC)
    {
        resetDefaultsFirst();
    }
    memcpy(RegHoldingBuf.raw, container.holdings.raw, sizeof(union unHoldings));
	memcpy(RegHoldingBuf_tmp.raw, container.holdings.raw, sizeof(union unHoldings));
    memcpy(CoilsBuf.raw, container.Coils.raw, sizeof(union unCoils));
	CLRWDT();
#ifndef MUTE_RESET
    memset(RegInputBuf.raw, 0, sizeof(union unInputs));
#endif
    memset(DiscretInputsBuf.raw, 0, sizeof (union unDiscretInputs));
}

void saveSettings()
{
    reset_saving();

    container_to_write.size = sizeof(container_to_write);
	// save before appling
	if (holdingValidator() == MB_ENOERR) {
        cancel_write_to_protected_cells();
		memcpy(container_to_write.holdings.raw, RegHoldingBuf_tmp.raw, sizeof(union unHoldings));
	} else {
    	memcpy(container_to_write.holdings.raw, RegHoldingBuf.raw, sizeof(union unHoldings));
    }
    memcpy(container_to_write.Coils.raw, CoilsBuf.raw, sizeof(union unCoils));

	container_to_write.Coils.names.InfinitySendMode = 0;
    
    saveSettingsContainer();
}

static void apply_save()
{
	applySettings();
	shadule(saveSettings);
}

void processCoils()
{
    if (CoilsBuf.names.ResetDefaults)
    {
        shadule(resetDefaults);
        return;
    }
    if (CoilsBuf.names.WriteSettings)
    {
        CoilsBuf.names.WriteSettings = 0;
		if (CoilsBuf.names.ApplySettings) {
			CoilsBuf.names.ApplySettings = 0;
			processAfterAnsver = apply_save;
		}
		else
        	processAfterAnsver = saveSettings;
		return;
    }
    if (CoilsBuf.names.ApplySettings)
    {
        CoilsBuf.names.ApplySettings = 0;
        processAfterAnsver = applySettings;
    }
}

void processCoils_spii2c()
{
    if (CoilsBuf.names.ResetDefaults)
    {
        shadule(resetDefaults);
        return;
    }
	if (CoilsBuf.names.ApplySettings)
    {
        CoilsBuf.names.ApplySettings = 0;
        applySettings();
    }
    if (CoilsBuf.names.WriteSettings)
    {
        CoilsBuf.names.WriteSettings = 0;
		saveSettings();
    }   
}

float convertkgs_m2_TO(float kgs_sm2)
{
    enum enMeasureUnits dst = 
        (enum enMeasureUnits)RegHoldingBuf.names.MeasureUnits;
    return dst == MU_kgs_cm2 ? kgs_sm2 : (kgs_sm2 * mmHgTOCoeffs[dst - 1]);
}
