/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <string.h>
#include <stdlib.h>

#include "mb.h"
#include "FreqMeters.h"
#include "dataModel.h"
#include "system.h"
#include "shaduler.h"
#include "user.h"

#include "core.h"

#define FILTER_ENABLED 			0
#define SUCCESS_LOOPS_NEEDED            3
#define CALC_OUTPUT_VALUE       1

struct Filter {
    const uint8_t size;
    const float const *coeffs;
    float *data;
    const float k;
};

static unsigned long maxU = ~0;
#define NAN  	(*((float*)&maxU))

static const float filterCoeffs[] = {
    0.08855240952789907,
    0.10950406583046077,
    0.1433572578657607,
    0.15619928299052657,
    0.1433572578657607,
    0.10950406583046077,
    0.08855240952789907,
};

#define FilterSumm  0.839026749

static float PFilter_data[sizeof (filterCoeffs) / sizeof (float) ];
static float TFilter_data[sizeof (filterCoeffs) / sizeof (float) ];

static struct Filter PFilter = {
    sizeof (filterCoeffs) / sizeof (float), filterCoeffs, PFilter_data, FilterSumm
};
static struct Filter TFilter = {
    sizeof (filterCoeffs) / sizeof (float), filterCoeffs, TFilter_data, FilterSumm
};

void initCore() {
    InitDataModel();
    FreqMetersEnable();
    enableShaduler(1);
}

static void RestartPresCounter(uint32_t target) {
    PresTimerTarget_new.value = target;
    pressureData.work = PresTimerTarget_new.s.Extantion;
    PresTimerTarget_new.s.REGISTER = (~PresTimerTarget_new.s.REGISTER) + 1;
    TMR0H = 0xff;
    NOP();
    TMR0L = 0xf0;
    T0CONbits.TMR0ON = 1; // start new cycle
    PresAliveCounter = ALIVE_COUNTER;
}

static void RestartTempCounter(uint32_t target) {
    TempTimerTarget_new.value = target;
    temperatureData.work = TempTimerTarget_new.s.Extantion;
    TempTimerTarget_new.s.REGISTER = (~TempTimerTarget_new.s.REGISTER) + 1;
    TMR1H = 0xff;
    NOP();
    TMR1L = 0xff;
    T1CONbits.TMR1ON = 1; // start new cycle
    TempAliveCounter = ALIVE_COUNTER;
}

void checkChanelsAlive() {
#if 1
    DiscretInputsBuf.names.PresureChanelFailure = 0;
    DiscretInputsBuf.names.TemperatureChanelFailure = 0;
    if (CoilsBuf.names.PowerSaveMode) {
        // no mesure - no errors ;)
        DiscretInputsBuf.names.PresureChanelFailure = 0;
        RegInputBuf.names.Fp = 0.0;
        RegInputBuf.names.Pressure = NAN;
        
        DiscretInputsBuf.names.TemperatureChanelFailure = 0;
        DiscretInputsBuf.names.CryticalTemperature = 0;
        RegInputBuf.names.Ft = 0.0;
        RegInputBuf.names.Temperature = NAN;
    } else {
        if (CoilsBuf.names.PresureEnabled) {
            if (!PresAliveCounter) {
                DiscretInputsBuf.names.PresureChanelFailure = 1; // error detected
                RegInputBuf.names.Pressure = NAN;
                RegInputBuf.names.Fp = 0.0;
                
                // force stop and restart
                T0CONbits.TMR0ON = 0;
                RestartPresCounter(1);
            } else
                --PresAliveCounter;
        } else {
            DiscretInputsBuf.names.PresureChanelFailure = 0;
            RegInputBuf.names.Fp = 0.0;
            RegInputBuf.names.Pressure = NAN;
        }

        if (CoilsBuf.names.TemperatureEnabled) {
            if (!TempAliveCounter) {
                DiscretInputsBuf.names.TemperatureChanelFailure = 1; // error detected
                RegInputBuf.names.Temperature = NAN;
                RegInputBuf.names.Ft = 0.0;
                
                // force stop and restart
                T1CONbits.TMR1ON = 0;
                RestartTempCounter(1);
            } else
                --TempAliveCounter;

            DiscretInputsBuf.names.CryticalTemperature =
                    RegInputBuf.names.Temperature >= CRITICAL_TEMPERATURE;
        } else {
            DiscretInputsBuf.names.TemperatureChanelFailure = 0;
            DiscretInputsBuf.names.CryticalTemperature = 0;
            RegInputBuf.names.Ft = 0.0;
            RegInputBuf.names.Temperature = NAN;
        }
    }
#endif
}

uint32_t calcTargetvalue(uint32_t F, uint16_t mesureTime_ms) {
    uint32_t res = (F * (uint32_t)mesureTime_ms) / 1000;
    return res ? res : 1;
}

static float fir_filter(struct Filter* filter, float curent) {
#if FILTER_ENABLED == 1
    memmove(&filter->data[1], &filter->data[0], (filter->size - 1) * sizeof (float));
    filter->data[0] = curent;
#if 0
    printf("{");
    for (uint8_t i = 0; i < filter->size; ++i) {
        printf("%f,", filter->data[i]);
    }
    printf("}\n");
#endif
    float result = 0;
    for (uint8_t i = 0; i < filter->size; ++i) {
        result += filter->data[i] * filter->coeffs[i];
    }
    return result / filter->k;
#else
    return curent;
#endif
}

static uint32_t resettarget(uint16_t measureTime) {
    return calcTargetvalue((uint32_t) START_FREQ,  measureTime);
}

static bool detctCatcheerror(uint16_t val) {
    return (val < 0xff);
}

static bool detectCatchErrors(union u16bitCounter data[2]) {
    return detctCatcheerror(data[0].value) || detctCatcheerror(data[1].value);
}

static uint32_t getResult(union u16bitCounter masterFreqData[2]) {
    uint32_t result = masterFreqData[1].value -  masterFreqData[0].value;
    if (result & (1ul << (sizeof (result) * 8 - 1)))
        result = (1ul << (sizeof (result) * 8 - 1)) -
            masterFreqData[0].value + masterFreqData[1].value;
    return result;
}

static uint32_t CalcNewTargetValue(bool isError, float target,
        float* F, uint8_t* Ready, uint16_t MesureTime, uint32_t startValue, 
        uint32_t *result) 
{
    if ((*result != startValue) && (!isError)) {
        *F = MASTER_CLOCK_HZ / ((float) (*result)) * target;
        *result = calcTargetvalue((uint32_t) *F,  MesureTime);
        if (*Ready < SUCCESS_LOOPS_NEEDED)
            (*Ready)++;
    } else {
        *result = resettarget(MesureTime);
        *Ready = 0;
    }
    
    return *result;
}

/* DEBUGER shows, what TMR0H fails to write, 8-bit mode set */
void CalcP() {
    if ((!T0CONbits.TMR0ON) &&
            CoilsBuf.names.PresureEnabled &&
            !CoilsBuf.names.PowerSaveMode) {
        uint32_t result;
        static uint32_t P_counter_target = 0;
        static uint8_t Redy = 0;
        float Fp;

        bool err = detectCatchErrors(pressureData.masterFreqData);
        if (err && P_counter_target) {
            RestartPresCounter(P_counter_target);
            return;
        }

        result = getResult(pressureData.masterFreqData);
        
        P_counter_target =
                CalcNewTargetValue(
                DiscretInputsBuf.names.PresureChanelFailure, 
                P_counter_target, &Fp, &Redy, RegHoldingBuf.names.PressureMesureTime,
                pressureData.masterFreqData[0].value, &result);
        
        RestartPresCounter(P_counter_target);

#if CALC_OUTPUT_VALUE
        // calc
        if (!DiscretInputsBuf.names.PresureChanelFailure
                && (Redy == SUCCESS_LOOPS_NEEDED)) {
            float Fp_f = fir_filter(&PFilter, Fp);

            struct sPressureCoeffs PressureCoeffs;
            memcpy(&PressureCoeffs, &RegHoldingBuf.names.P_coeffs,
                    sizeof (struct sPressureCoeffs));

            float Ft_minus_Ft0;
            if (CoilsBuf.names.TemperatureEnabled)
                Ft_minus_Ft0 = RegInputBuf.names.Ft - PressureCoeffs.Ft0;
            else
                Ft_minus_Ft0 = 0;
            float PresF_minus_Fp0 = Fp_f - PressureCoeffs.Fp0;

            //processTimeCrytical();

            float Pressure_fSens = 0;
#if 1
           {
                float k0 = PressureCoeffs.A[0] + Ft_minus_Ft0 *
                        (PressureCoeffs.A[1] + Ft_minus_Ft0 *
                        (PressureCoeffs.A[2] + Ft_minus_Ft0 * PressureCoeffs.A[12]));
                float k1 = PressureCoeffs.A[3] + Ft_minus_Ft0 *
                        (PressureCoeffs.A[5] + Ft_minus_Ft0 *
                        (PressureCoeffs.A[7] + Ft_minus_Ft0 * PressureCoeffs.A[13]));
                float k2 = PressureCoeffs.A[4] + Ft_minus_Ft0 *
                        (PressureCoeffs.A[6] + Ft_minus_Ft0 *
                        (PressureCoeffs.A[8] + Ft_minus_Ft0 * PressureCoeffs.A[14]));
                float k3 = PressureCoeffs.A[9] + Ft_minus_Ft0 *
                        (PressureCoeffs.A[10] + Ft_minus_Ft0 *
                        (PressureCoeffs.A[11] + Ft_minus_Ft0 * PressureCoeffs.A[15]));
                Pressure_fSens = k0 + PresF_minus_Fp0 * 
                        (k1 + PresF_minus_Fp0 * ( k2 + PresF_minus_Fp0 * k3));
            }
#else
            {
                float PresF_minus_Fp0_2 = PresF_minus_Fp0 * PresF_minus_Fp0;
                float PresF_minus_Fp0_3 = PresF_minus_Fp0_2 * PresF_minus_Fp0;

                float Ft_minus_Ft0_2 = Ft_minus_Ft0 * Ft_minus_Ft0;
                float Ft_minus_Ft0_3 = Ft_minus_Ft0_2 * Ft_minus_Ft0;
                
                //processTimeCrytical();

                float _A[16];
                Pressure_fSens = PressureCoeffs.A[0];
                _A[15] = PressureCoeffs.A[15] * PresF_minus_Fp0_3 * Ft_minus_Ft0_3;
                _A[14] = PressureCoeffs.A[14] * PresF_minus_Fp0_2 * Ft_minus_Ft0_3;
                _A[13] = PressureCoeffs.A[13] * PresF_minus_Fp0 * Ft_minus_Ft0_3;

                _A[11] = PressureCoeffs.A[11] * PresF_minus_Fp0_3 * Ft_minus_Ft0_2;
                _A[10] = PressureCoeffs.A[10] * PresF_minus_Fp0_3 * Ft_minus_Ft0;

                _A[8] = PressureCoeffs.A[8] * PresF_minus_Fp0_2 * Ft_minus_Ft0_2;
#if 0
                _A[7] = PressureCoeffs.A[7] * Ft_minus_Ft0 * PresF_minus_Fp0_2;
                _A[6] = PressureCoeffs.A[6] * Ft_minus_Ft0_2 * PresF_minus_Fp0;
#else
                _A[7] = PressureCoeffs.A[7] * PresF_minus_Fp0 * Ft_minus_Ft0_2;
                _A[6] = PressureCoeffs.A[6] * PresF_minus_Fp0_2 * Ft_minus_Ft0;
#endif
                _A[5] = PressureCoeffs.A[5] * PresF_minus_Fp0 * Ft_minus_Ft0;

                //processTimeCrytical();

                _A[1] = PressureCoeffs.A[1] * Ft_minus_Ft0;
                _A[2] = PressureCoeffs.A[2] * Ft_minus_Ft0_2;
                _A[12] = PressureCoeffs.A[12] * Ft_minus_Ft0_3;

                _A[3] = PressureCoeffs.A[3] * PresF_minus_Fp0;
                _A[4] = PressureCoeffs.A[4] * PresF_minus_Fp0_2;
                _A[9] = PressureCoeffs.A[9] * PresF_minus_Fp0_3;

                //processTimeCrytical();

                for (uint8_t i = 1; i < sizeof(_A) / sizeof(float); ++i)
                    Pressure_fSens += _A[i];
            }       
#endif
            RegInputBuf.names.Pressure = convertkgs_m2_TO(Pressure_fSens);
            RegInputBuf.names.Fp = Fp_f;
        }
#else
        RegInputBuf.names.Fp = Fp;
        RegInputBuf.names.Pressure = result;
#endif
    }
}

void CalcT() {
    if ((!T1CONbits.TMR1ON) &&
            CoilsBuf.names.TemperatureEnabled &&
            !CoilsBuf.names.PowerSaveMode) {
        uint32_t result;
        static uint32_t T_counter_target = 0;
        static uint8_t Redy = 0;
        float Ft;
        
        bool err = detectCatchErrors(temperatureData.masterFreqData);
        if (err && T_counter_target) {
            RestartTempCounter(T_counter_target);
            return;
        }

        result = getResult(temperatureData.masterFreqData);
        
        T_counter_target = CalcNewTargetValue(
                DiscretInputsBuf.names.TemperatureChanelFailure, 
                T_counter_target, &Ft, &Redy, RegHoldingBuf.names.TemperatureMesureTime,
                temperatureData.masterFreqData[0].value, &result);
        
        RestartTempCounter(T_counter_target);

        //processTimeCrytical();
        
#if CALC_OUTPUT_VALUE
        // calc
        if (!DiscretInputsBuf.names.TemperatureChanelFailure
                && (Redy == SUCCESS_LOOPS_NEEDED)) {
            // filter nidddles
            float Ft_f = fir_filter(&TFilter, Ft);
            struct sTemperatureCoeffs TemperatureCoeffs;
            memcpy(&TemperatureCoeffs, &RegHoldingBuf.names.T_coeffs,
                    sizeof (struct sTemperatureCoeffs));
            float TempF_minus_Ft0 = Ft - TemperatureCoeffs.F0;
            float _temp = TempF_minus_Ft0;
            float Temperature_fSens = TemperatureCoeffs.T0;

            //processTimeCrytical();

            for (unsigned char i = 0; i < T_COEFFS_COUNT; ++i, 
                    _temp *= TempF_minus_Ft0) {
                Temperature_fSens += TemperatureCoeffs.C[i] * _temp;
            }

            RegInputBuf.names.Temperature = Temperature_fSens;
            RegInputBuf.names.Ft = Ft_f;
        }
#else
        RegInputBuf.names.Ft = Ft;
        RegInputBuf.names.Fp = result;
#endif
    }
}

void genPrescalerPostscalerPlank(uint32_t Kd, uint8_t* PrescalerVal,
        uint8_t *postscalerVal, uint8_t *plank) {
    static const uint8_t prescalers[] = {1, 4, 16};
    uint8_t postscaler;
    uint8_t i;
    uint8_t _plank = 0xff;

    for (i = 0; i < sizeof (prescalers); ++i)
        for (postscaler = 1; postscaler <= 16; ++postscaler) {
            uint16_t need_dev = Kd / (prescalers[i] * postscaler);
            if (need_dev < 256) {
                _plank = need_dev;
                goto __end_genPrescalerPostscalerStart;
            }
        }

__end_genPrescalerPostscalerStart:
    *PrescalerVal = i;
    *postscalerVal = postscaler - 1;
    *plank = _plank;
}
