/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <string.h>

#include "system.h"
#include "core.h"
#include "infintySender.h"
#include "dataModel.h"
#include "shaduler.h"
#include "port.h"

#define MIN(x, y)	((x < y) ? x : y)

// using TIMER4 for shaduling on 1ms

//#define NO_SEND
//#define TOGGLE

#ifdef TOGGLE
static void toggle()
{
	TRISCbits.TRISC6 = 0;
	LATCbits.LATC6 = !LATCbits.LATC6;
}
#endif

static struct Task tasks[] =
{
#ifndef NO_SEND
    {
        20, sendReport
    },
#endif          
#if 1
	{
		400, checkChanelsAlive
	}
#endif
#ifdef TOGGLE
	,
	{
		2, toggle
	}
#endif
    ,
    {
        1, save_byte
    },
};

static volatile  struct Task tasks_work[sizeof(tasks)/sizeof(struct Task)];

#define MODULE_CLOCK            (FCY)
#define TARGET_COUNTER_TICKS_MS (MODULE_CLOCK / 1000)

static void reloadTask(uint8_t id)
{
    memcpy(&tasks_work[id], & tasks[id], sizeof(struct Task));
}

void enableShaduler(BOOL enable)
{
    if (enable)
    {
		uint8_t postscaler;
        uint8_t prescaler;
#ifndef NO_SEND
        tasks[0].period = RegHoldingBuf.names.cyclicalSendPeriod;
#endif
    
        for (prescaler = 0; prescaler < sizeof(tasks)/sizeof(struct Task);
                ++prescaler)
            reloadTask(prescaler);

        // autoselect prescaler and postscaler
        genPrescalerPostscalerPlank(TARGET_COUNTER_TICKS_MS, &prescaler,
            &postscaler, (unsigned char*)&PR4);

        T4CON = 0;                
                        
        T4CONbits.T4OUTPS = postscaler;
        T4CONbits.T4CKPS = prescaler;

        TMR4 = 0;

        IPR4bits.TMR4IP = 0; // low prio
        PIR4bits.TMR4IF = 0;
        PIE4bits.TMR4IE = 1;

        T4CONbits.TMR4ON = 1; // enable
    }
    else
    {
        T4CONbits.TMR4ON = 0;
        PIE4bits.TMR4IE = 0;
    }
}


void tick()
{  
#if 1
    for (uint8_t TaskID = 0; TaskID < sizeof(tasks)/sizeof(struct Task);
            ++TaskID)
    {
        taskRoutine Routine = tasks_work[TaskID].Routine;
        if (Routine && (--tasks_work[TaskID].period == 0))
        {
			tasks_work[TaskID].period = tasks[TaskID].period; // reload
			shadule(Routine);
        }
    }
#endif
}

////////////////////////////////////////////////////////////////////////////////

#define task_queue_size 16

struct sTaskBuf {
    uint8_t count;
    taskRoutine buf[task_queue_size];
};

static volatile struct sTaskBuf TaskQeues[2];
static volatile uint8_t buf_selector = 0;

void ProcessShaduled()
{
    uint8_t count = TaskQeues[buf_selector].count;
    if (count) {
        asm("btg    _buf_selector, 0"); // swap buffers atomic
    
        uint8_t work_buf = buf_selector ^ 1; // work buffer now can not be modified
        
        count = TaskQeues[work_buf].count;
        TaskQeues[work_buf].count = 0; // reset buffer
        for (uint8_t i = 0; i < count; ++i) {
            static taskRoutine r;
            CLRWDT();
            r = TaskQeues[work_buf].buf[i];
            r();
        }
    }
}

BOOL shadule(taskRoutine routine)
{
    volatile uint8_t* pCount = &TaskQeues[buf_selector].count; 
    if (*pCount < task_queue_size) {
        TaskQeues[buf_selector].buf[*pCount] = routine;
        (*pCount)++;
        return TRUE;
    }
    return FALSE;
}
