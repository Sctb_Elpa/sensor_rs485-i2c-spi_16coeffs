#!/usr/bin/env python

import os, sys
import ntpath
import re
import subprocess

basepath = os.getcwd()


def show_usage():
	print("{} <dest file.c> <src.in> <VERSIONFILE>".format(sys.argv[0]))


def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def extract_next_version(text):
	m = re.findall("Next (\\d+).(\\d+).(\\d+).(\\d+)", text, re.MULTILINE)
	if not m:
		return 0x000000, ""
	
	vi = tuple(map(int, m[0]))
	print(vi)
	
	lines = text.split('\n')
	rest = '\n'.join(lines[:-2]) if len(lines) > 1 else ""
		
	return (vi[0] << 24) | (vi[1] << 16) | (vi[2] << 8) | vi[3], rest
	
	
def git_get_head_hash():
	return subprocess.check_output(["git", "rev-parse", "--verify", "HEAD"]).strip().decode()
	
	
def print_version(v):
	v1 = v >> 24;
	v2 = (v >> 16) & 0xFF;
	v3 = (v >> 8) & 0xFF;
	v4 = v & 0xFF;	
	return "{}.{}.{}.{}".format(v1, v2, v3, v4)


def main():
	if len(sys.argv) != 4:
		show_usage()
		exit(-1)
		
	dest_file = sys.argv[1].replace('\\', '\\\\')
	
	dest_path = ntpath.dirname(dest_file)
	if not os.path.exists(dest_path):
		print("Creating subdir {}".format(dest_path))
		os.makedirs(dest_path)
	
	with open(sys.argv[2], 'r') as src, open(sys.argv[3], 'r+') as vf:
		src_data = src.read()
		vfdata = vf.read();
		
		current_version, rvfc = extract_next_version(vfdata);
		nextversion = (current_version & 0xFFFFFF00) | ((current_version + 1) & 0xFF)
		commit = git_get_head_hash()
		commit_short = commit[:8]
		
		vf.seek(0)
		vf.write(rvfc)
		vf.write('\n')
		vf.write("{} - {}\n".format(print_version(current_version), commit))
		vf.write("Next {}\n".format(print_version(nextversion)))
		
		src_data = src_data\
			.replace("AAAAAAAA", "{:08X}".format(current_version))\
			.replace("BBBBBBBB", commit_short);
		
		print("dest_file: {}".format(dest_file))
		with open(dest_file, 'w') as df:
			df.write(src_data)
	
	
if __name__ == '__main__':
    main()