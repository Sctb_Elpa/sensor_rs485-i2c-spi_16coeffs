#include <stdio.h>
#include <stdint.h>

typedef float T;

static const T mmHgTOCoeffs[] = {
    735.55914,
    980.665012,
    98.0665012,
    0.0980665012,
    10000.0,
    14.223343
};

struct {
    T A[16];
    T Ft0;
    T Fp0;
} PressureCoeffs = {
    /*{
        0.611001727884195,
        1.54254888490519E-05,
        -1.15686814470347E-07,
        0.000460461616015,
        7.23974810827676E-12,
        2.41319420173968E-09,
        4.36575872446478E-11,
        2.28674209425231E-10,
        -4.21027662005695E-13,
        1.82206040608995E-12,
        -2.79330080207083E-14,
        2.64537804916779E-16,
        -1.02874050364716E-11,
        -7.16940659755904E-13,
        1.20057314284309E-15,
        -7.27153433072834E-19,
    },*/
{
    449.427808213106,
    0.011346121380484,
    -8.50932957519035E-05,
    0.338697028201068,
    5.16911846659696E-09,
    1.7760182231541E-06,
    3.21116180312941E-08,
    1.68199428607352E-07,
    -3.09687619298726E-10,
    1.34020649829661E-09,
    -2.05460100972596E-11,
    1.94582920942979E-13,
    -7.56611884256131E-09,
    -5.27358341023687E-10,
    8.8310372334038E-13,
    -5.34870270374508E-16,
},
32679, 917
};

T P_method1(T fp, T ft) {
    //𝑃0+
    //A1∙(Ft−Ft0)+
    //A2∙(Ft−Ft0)^2+
    //A3∙(Fp−Fp0)+
    //A4∙(Fp−Fp0)^2+
    //A5∙(Fp−Fp0)∙(Ft−Ft0)+
    //A6∙(Ft−Ft0)^2∙(Fp−Fp0)+
    //A7∙(Ft−Ft0)∙(Fp−Fp0)^2+
    //A8∙(Ft−Ft0)^2∙(Fp−Fp0)^2+
    //A9∙(Fp−Fp0)^3+
    //A10∙(Ft−Ft0)∙(Fp−Fp0)^3+
    //A11∙(Ft−Ft0)^2∙(Fp−Fp0)^3+
    //A12∙(Ft−Ft0)^3+
    //A13∙(Ft−Ft0)^3∙(Fp−Fp0)+
    //A14∙(Ft−Ft0)^3∙(Fp−Fp0)^2+
    //A15∙(Ft−Ft0)^3∙(Fp−Fp0)^3


    //P = P0+A1*Ft+A2*Ft^2+A3*Fp+A5*Fp*Ft+A6*Ft^2*Fp+A7*Ft*Fp^2+A8*Ft^2*Fp^2+A9*Fp^3+A10*Ft*Fp^3+A11*Ft^2*Fp^3+A12*Ft^3+A13*Ft^3*Fp+A14*Ft^3*Fp^2+A15*Ft^3*Fp^3

    T Ft_minus_Ft0 = ft - PressureCoeffs.Ft0;
    T PresF_minus_Fp0 = fp - PressureCoeffs.Fp0;

    T PresF_minus_Fp0_2 = PresF_minus_Fp0 * PresF_minus_Fp0;
    T PresF_minus_Fp0_3 = PresF_minus_Fp0_2 * PresF_minus_Fp0;

    T Ft_minus_Ft0_2 = Ft_minus_Ft0 * Ft_minus_Ft0;
    T Ft_minus_Ft0_3 = Ft_minus_Ft0_2 * Ft_minus_Ft0;

    T _A[16];
    T Pressure_fSens = PressureCoeffs.A[0];
    _A[15] = PressureCoeffs.A[15] * PresF_minus_Fp0_3 * Ft_minus_Ft0_3;
    _A[14] = PressureCoeffs.A[14] * PresF_minus_Fp0_2 * Ft_minus_Ft0_3;
    _A[13] = PressureCoeffs.A[13] * PresF_minus_Fp0 * Ft_minus_Ft0_3;

    _A[11] = PressureCoeffs.A[11] * PresF_minus_Fp0_3 * Ft_minus_Ft0_2;
    _A[10] = PressureCoeffs.A[10] * PresF_minus_Fp0_3 * Ft_minus_Ft0;

    _A[8] = PressureCoeffs.A[8] * PresF_minus_Fp0_2 * Ft_minus_Ft0_2;

#if 0
    //_A[7] = PressureCoeffs.A[7] * Ft_minus_Ft0 * PresF_minus_Fp0_2;
    //_A[6] = PressureCoeffs.A[6] * Ft_minus_Ft0_2 * PresF_minus_Fp0;
#else
    _A[7] = PressureCoeffs.A[7] * PresF_minus_Fp0 * Ft_minus_Ft0_2;
    _A[6] = PressureCoeffs.A[6] * PresF_minus_Fp0_2 * Ft_minus_Ft0;
#endif
    _A[5] = PressureCoeffs.A[5] * PresF_minus_Fp0 * Ft_minus_Ft0;

    _A[1] = PressureCoeffs.A[1] * Ft_minus_Ft0;
    _A[2] = PressureCoeffs.A[2] * Ft_minus_Ft0_2;
    _A[12] = PressureCoeffs.A[12] * Ft_minus_Ft0_3;

    _A[3] = PressureCoeffs.A[3] * PresF_minus_Fp0;
    _A[4] = PressureCoeffs.A[4] * PresF_minus_Fp0_2;
    _A[9] = PressureCoeffs.A[9] * PresF_minus_Fp0_3;

    printf("_A[%d]=%f\n", 0, Pressure_fSens);
    for (uint8_t i = 1; i < sizeof(_A) / sizeof(T); ++i) {
        Pressure_fSens += _A[i];
        printf("_A[%d]=%f\n", i, _A[i]);
    }

    return Pressure_fSens;
}

T P_method2(T fp, T ft) {
    T Ft_minus_Ft0 = ft - PressureCoeffs.Ft0;
    T PresF_minus_Fp0 = fp - PressureCoeffs.Fp0;

    T PresF_minus_Fp0_2 = PresF_minus_Fp0 * PresF_minus_Fp0;
    T PresF_minus_Fp0_3 = PresF_minus_Fp0_2 * PresF_minus_Fp0;

    T Ft_minus_Ft0_2 = Ft_minus_Ft0 * Ft_minus_Ft0;
    T Ft_minus_Ft0_3 = Ft_minus_Ft0_2 * Ft_minus_Ft0;

    T _A[16];
    T Pressure_fSens = PressureCoeffs.A[0];
    _A[15] = PressureCoeffs.A[15] * PresF_minus_Fp0_3 * Ft_minus_Ft0_3;
    _A[14] = PressureCoeffs.A[14] * PresF_minus_Fp0_2 * Ft_minus_Ft0_3;
    _A[13] = PressureCoeffs.A[13] * PresF_minus_Fp0 * Ft_minus_Ft0_3;

    _A[11] = PressureCoeffs.A[11] * PresF_minus_Fp0_3 * Ft_minus_Ft0_2;
    _A[10] = PressureCoeffs.A[10] * PresF_minus_Fp0_3 * Ft_minus_Ft0;

    _A[8] = PressureCoeffs.A[8] * PresF_minus_Fp0_2 * Ft_minus_Ft0_2;
//
    _A[7] = PressureCoeffs.A[6] * Ft_minus_Ft0 * PresF_minus_Fp0_2;
    _A[6] = PressureCoeffs.A[7] * Ft_minus_Ft0_2 * PresF_minus_Fp0;
//
    _A[5] = PressureCoeffs.A[5] * PresF_minus_Fp0 * Ft_minus_Ft0;

    _A[1] = PressureCoeffs.A[1] * Ft_minus_Ft0;
    _A[2] = PressureCoeffs.A[2] * Ft_minus_Ft0_2;
    _A[12] = PressureCoeffs.A[12] * Ft_minus_Ft0_3;

    _A[3] = PressureCoeffs.A[3] * PresF_minus_Fp0;
    _A[4] = PressureCoeffs.A[4] * PresF_minus_Fp0_2;
    _A[9] = PressureCoeffs.A[9] * PresF_minus_Fp0_3;

    printf("_A[%d]=%f\n", 0, Pressure_fSens);
    for (uint8_t i = 1; i < sizeof(_A) / sizeof(T); ++i) {
        Pressure_fSens += _A[i];
        printf("_A[%d]=%f\n", i, _A[i]);
    }

    return Pressure_fSens;
}

T P_method3(T fp, T ft) {
    T Ft_minus_Ft0 = ft - PressureCoeffs.Ft0;
    T PresF_minus_Fp0 = fp - PressureCoeffs.Fp0;

    T k0 = PressureCoeffs.A[0] + Ft_minus_Ft0 *
            (PressureCoeffs.A[1] + Ft_minus_Ft0 *
            (PressureCoeffs.A[2] + Ft_minus_Ft0 * PressureCoeffs.A[12]));
    T k1 = PressureCoeffs.A[3] + Ft_minus_Ft0 *
            (PressureCoeffs.A[5] + Ft_minus_Ft0 *
            (PressureCoeffs.A[7] + Ft_minus_Ft0 * PressureCoeffs.A[13]));
    T k2 = PressureCoeffs.A[4] + Ft_minus_Ft0 *
            (PressureCoeffs.A[6] + Ft_minus_Ft0 *
            (PressureCoeffs.A[8] + Ft_minus_Ft0 * PressureCoeffs.A[14]));
    T k3 = PressureCoeffs.A[9] + Ft_minus_Ft0 *
            (PressureCoeffs.A[10] + Ft_minus_Ft0 *
            (PressureCoeffs.A[11] + Ft_minus_Ft0 * PressureCoeffs.A[15]));

    printf(">k0 = %0.9f\n>k1 = %0.9f\n>k2 = %0.9f\n>k3 = %0.9f\n", k0, k1, k2, k3);

    return k0 + PresF_minus_Fp0 * (k1 + PresF_minus_Fp0 * ( k2 + PresF_minus_Fp0 * k3));
}

T convertkgs_m2_TO(T kgs_sm2)
{
    return kgs_sm2 * mmHgTOCoeffs[0];
}

int main()
{
    T fp = 1785.703;
    T ft = 32733.43;

    T p_1 = P_method1(fp, ft);
    T pmmHg_1 = convertkgs_m2_TO(p_1);

    printf("Method 1: Fp = %f, Ft = %f: P = %f, P[mmHg] = %f\n", fp, ft, p_1, pmmHg_1);

    T p_2 = P_method2(fp, ft);
    T pmmHg_2 = convertkgs_m2_TO(p_2);

    printf("Method 2: Fp = %f, Ft = %f: P = %f, P[mmHg] = %f\n", fp, ft, p_2, pmmHg_2);

    T p_3 = P_method3(fp, ft);
    T pmmHg_3 = convertkgs_m2_TO(p_2);

    printf("Method 3: Fp = %f, Ft = %f: P = %f, P[mmHg] = %f\n", fp, ft, p_3, pmmHg_3);
}
