/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* High-priority service */
#include "FreqMeters.h"
#include "port.h"
#include "user.h"

#include "spi_io.h"

#include "i2c_io.h"
#include "shaduler.h"
#include "mbport.h"
#include "mb.h"

#include "dataModel.h"

static union u16bitCounter refVal;

#if defined(__XC) || defined(HI_TECH_C)
void interrupt high_isr(void)
#elif defined (__18CXX)
#pragma code high_isr=0x08
#pragma interrupt high_isr
void high_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{
#if FREQMETER_ISR_PRIO == 1
    CATCH_MASTER_TIMER();

    // Temperature freq counter ISR
    if(PIR1bits.TMR1IF)
    {
        TEMPERATURE_TIMER_ISR();
        //PIR1bits.TMR1IF = 0;
    }

    // Pressute freq counter ISR
    if(INTCONbits.TMR0IF)
    {
        PRESSURE_TIMER_ISR();
        //INTCONbits.TMR0IF = 0;
    }
#else
    if(PIR2bits.TMR3IF) {
        REF_TIMER_ISR();
        PIR2bits.TMR3IF = 0;
    }
#endif

#if SPI_HIGH_PRIO == 1
    // spi
    if (PIE1bits.SSPIE && PIR1bits.SSPIF)
    { 
	PIR1bits.SSPIF = 0;
        SSP_ISR();
    }
#endif

    // spi mode detection
    if(INTCONbits.INT0IE && INTCONbits.INT0IF)
    {
        INTCONbits.INT0IF = 0;
        pINT0_ISR();
    }
    
    if (PIR2bits.TMR3IF || (INTCONbits.TMR0IF && PIR1bits.TMR1IF)) {

        // force restart both chanels
        T0CONbits.TMR0ON = 0; 
        T1CONbits.TMR1ON = 0; 

        pressureData.masterFreqData[0].s.REGISTER =
            temperatureData.masterFreqData[0].s.REGISTER = 0;
    }
    PIR1bits.TMR1IF = 0;
    INTCONbits.TMR0IF = 0;
}

extern void vMBTIMERExpiredISR( void ); // modbus timer ISR

#define ENABLE_NESTED_ISR(routine)	\
	do {\
		INTCONbits.GIEL = 1; \
		routine(); \
		INTCONbits.GIEL = 0; \
	} while (0)

/* Low-priority interrupt routine */
#if defined(__XC) || defined(HI_TECH_C)
void low_priority interrupt low_isr(void)
#elif defined (__18CXX)
#pragma code low_isr=0x18
#pragma interruptlow low_isr
void low_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{
    // w/o this syncroniusly niddle on P and T
    INTCONbits.TMR0IE = 0;
    PIE1bits.TMR1IE = 0;
    if(PIR2bits.TMR3IF)
    {
        PIR2bits.TMR3IF = 0;
        REF_TIMER_ISR();
    }
    INTCONbits.TMR0IE = 1;
    PIE1bits.TMR1IE = 1;

#if FREQMETER_ISR_PRIO == 0
    union u16bitCounter refVal;

    T3CONbits.TMR3ON = 0;
    refVal.s.REGISTER = TMR3;
    refVal.s.Extantion = RefFreqTimer_Ovf;
    T3CONbits.TMR3ON = 1;
        
     // Pressute freq counter ISR
    if(INTCONbits.TMR0IE && INTCONbits.TMR0IF)
    {
        PRESSURE_TIMER_ISR();
        INTCONbits.TMR0IF = 0;
    }
    // Temperature freq counter ISR
    if(PIE1bits.TMR1IE && PIR1bits.TMR1IF)
    {
        TEMPERATURE_TIMER_ISR();
        PIR1bits.TMR1IF = 0;
    }
#endif

    // i2c/spi
    if (PIE1bits.SSPIE && PIR1bits.SSPIF)
    { 
        PIR1bits.SSPIF = 0;
        SSP_ISR();
    }

    // UART interrupts
    if(PIE3bits.RC2IE && PIR3bits.RC2IF)
    {
        PIR3bits.RC2IF = 0;
        pxMBFrameCBByteReceived();
    }
    if(PIE3bits.TX2IE && PIR3bits.TX2IF)
    {
        PIR3bits.TX2IF = 0;
        pxMBFrameCBTransmitterEmpty();
    }

    // modbus timeout interrupt
    if (PIE1bits.TMR2IE && PIR1bits.TMR2IF)
    {
        PIR1bits.TMR2IF = 0;
        vMBTIMERExpiredISR();
    }

    //shaduler
    if (PIE4bits.TMR4IE && PIR4bits.TMR4IF)
    {
        PIR4bits.TMR4IF = 0;
        tick();
    }
}
