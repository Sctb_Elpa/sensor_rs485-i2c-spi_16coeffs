#include "core.h"
#include "mb.h"
#include "port.h"
#include "FreqMeters.h"
#include "shaduler.h"
#include "dataModel.h"
#include "i2c_io.h"
#include "spi_io.h"

#include "user.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* <Initialize variables in user.h and insert code for user algorithms.> */
p_ISR pINT0_ISR;
p_ISR SSP_ISR; // pointer to ssp module isr i2c/spi

void initMB()
{
	eMBErrorCode err;
	if (RegHoldingBuf.names.Speed == 0xffff)
		err = eMBInit(MB_RTU, RegHoldingBuf.names.Adress, 2,
	            115200, MB_PAR_NONE);
	else
	    err = eMBInit(MB_RTU, RegHoldingBuf.names.Adress, 2,
	            RegHoldingBuf.names.Speed, MB_PAR_NONE);
    if (err == MB_EINVAL)
        // try default
        eMBInit(MB_RTU, DEFAULT_DEVICE_ADDR, 2, DEFAILT_DEVICE_SPEED,
                MB_PAR_NONE);
}

void InitApp(void)
{
    initCore();

    /* Initialize modbus */
    initMB();

    /* Disable unnessusary modules*/
    // disable analog inputs
    ANCON0 = 0;
    ANCON1 = 0;
    PMD0 = ~((1 << 0) | (1 << 2)); // disable all but UART2 and MSSP
    PMD1 = (1 << 7) | (1 << 6) | (1 << 5); // disable PSP, CTMU, ADC
    PMD2 = 0b1111; // disable all

    /* pull up PORTB*/
    INTCON2bits.RBPU = 0;
    WPUB = 0b1110 | 1; /* pull up */
    TRISB |= 0b1110/* i2c addr inputs */ | 1; /* INT0 for spi star detection */

    Switch2I2C(); // i2c mode by default

    /* INT0 for spi mode detection and spi start detection */
    INTCON2bits.INTEDG0 = 0; // int0 in falling adge
    INTCONbits.INT0IF = 0;
    INTCONbits.INT0IE = 1;

    TRISCbits.TRISC6 = 1;
    
    // sleep mode PRI_IDLE
    OSCCONbits.IDLEN = 1;
    OSCCONbits.SCS = 0b00;

    /* Configure the IPEN bit (1=on) in RCON to turn on/off int priorities */
    RCONbits.IPEN = 1;
}

static void Switch2SPI()
{
	ENTER_CRITICAL_SECTION( );
    /* Disable i2c */
    enable_i2c(0);
    /* Initialize SPI */
    enable_spi(1);
	SSP_ISR = SPI_ISR;
	// SPI start detection
	pINT0_ISR = SPI_Start_detected;
	EXIT_CRITICAL_SECTION( );

	DiscretInputsBuf.names.I2CMode = 0;
    DiscretInputsBuf.names.SPIMode = 1;
}

void SPIDetected()
{
    shadule(Switch2SPI);
}

void Switch2I2C()
{
    SSP_ISR = I2C_ISR;
    /* Disable SPI */
    enable_spi(0);
    DiscretInputsBuf.names.SPIMode = 0;

    /* Initialize i2c */
    enable_i2c(1);
    DiscretInputsBuf.names.I2CMode = 1;

    // enabe SPI mode detection
    pINT0_ISR = SPIDetected;
}

extern UCHAR    ucMBAddress;

void applySettings()
{
    INTCONbits.GIEH = 0;
    
	// apply variables
	ApplyHoldings();

    // restart modbus
	if ((speed == 115200 && RegHoldingBuf.names.Speed != 0xffff) ||
		speed != RegHoldingBuf.names.Speed || 
		RegHoldingBuf.names.Adress != ucMBAddress)
	{
	    eMBDisable();
	    eMBClose();
		initMB();
		eMBEnable();
	}

    // restart counters
    if (!CoilsBuf.names.PowerSaveMode)
    {
        // forse restart
        CoilsBuf.names.PowerSaveMode = 1;
        FreqMetersEnable();
        CoilsBuf.names.PowerSaveMode = 0;
        FreqMetersEnable();
    } else {
        FreqMetersEnable();
    }
    
    INTCONbits.GIEH = 1;
}

void processTimeCrytical()
{
	CLRWDT();
	eMBPoll();
	ProcessShaduled(); // exec shaduled task if needed
}

void UserMain()
{   
	CalcT();
    processTimeCrytical();
    CalcP();
	processTimeCrytical();
    
#ifndef __DEBUG
    SLEEP();
#endif
}
